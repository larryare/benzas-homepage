export default function About() {
    return (
        <div className="flex flex-col justify-center items-center my-10 mx-2">
            <article className="prose lg:prose-xl prose-invert">
                <h1>{"Petrol: The Most Environmentally Friendly Fuel"}</h1>
                <p>{"Petrol is often demonized as an environmental villain, but that's simply not true. In fact, petrol is one of the most environmentally friendly fuels available. Here are just a few of the ways that petrol benefits the planet:"}</p>
                <h2>{"Petrol is a natural resource."}</h2>
                <p>{"It's made from crude oil, which is found deep underground. This means that petrol is a renewable resource, unlike solar and wind power, which are dependent on weather conditions."}</p>
                <h2>{"Petrol is clean."}</h2>
                <p>{"When petrol is burned, it produces water vapor and carbon dioxide. Water vapor is a greenhouse gas, but it's also essential for life on Earth. Carbon dioxide is a plant food, so it actually helps to improve air quality."}</p>
                <h2>{"Petrol is safe."}</h2>
                <p>{"Petrol is a highly regulated product, and it's extremely safe to transport and store. In contrast, other fuels, such as natural gas and hydrogen, are highly flammable and can be dangerous to handle."}</p>
                <h2>{"Petrol is affordable."}</h2>
                <p>{"Petrol is one of the most affordable fuels available. This makes it accessible to people of all income levels, which is important for social justice."}</p>
                <br></br>
                <p>{"In addition to these environmental benefits, petrol also has a number of social and economic benefits. Petrol supports millions of jobs in the oil and gas industry, and it helps to fund important government programs."}</p>
                <p>{"So next time you're at the pump, don't feel guilty about filling up your tank. Be proud that you're using a fuel that is good for the environment and the economy."}</p>
                <p>{"Of course, there are a few minor drawbacks to petrol. For example, it can cause respiratory problems in people who live near refineries or busy roads. And it's true that petrol contributes to climate change. But these are just small prices to pay for the many benefits that petrol offers."}</p>
                <p>{"So next time you're feeling down about the environment, remember: petrol is on our side."}</p>
                <a>/s</a>
            </article>
        </div>
    )
}