"use client";
import Link from "next/link";
import { usePathname } from "next/navigation";

function NavLink({ ...props }) {
  const router = usePathname();
  return (
    <Link
      href={props.href}
      className={
        router === props.href
          ? "text-rose-600 font-bold underline align-middle hover:bg-gray-300 rounded-lg px-2"
          : "text-purple-400 align-middle hover:bg-rose-200 hover:text-rose-800 rounded-lg px-2"
      }
    >
      {props.children}
    </Link>
  );
}

export default function Navbar() {
  return (
    <div className="flex fixed top-0 right-0 flex-row justify-end items-center m-2 h-8 rounded-lg backdrop-blur-lg">
      <NavLink href="/">Home</NavLink>
      <NavLink href="/about">About</NavLink>
      <NavLink href="/chebramobilis">ChebraMobilis</NavLink>
    </div>
  );
}
