import Link from "next/link";


export default function Home() {
  const gasPrice = Math.round(Math.random() * 100);
  return (
    <main className="h-screen flex flex-col justify-center items-center">
      <p className="text-8xl text-gray-400 hover:text-red-400">€1.{gasPrice}</p>
      <Link href={"https://mastodon.benzas.lt/invite/7NbdmFBx"} className="text-3xl text-pink-500 my-8 underline hover:text-pink-300">Join Benzas</Link>
      <form
        action="https://www.paypal.com/donate"
        method="post"
        target="_top"
        className="absolute bottom-4"
      >
        <input type="hidden" name="hosted_button_id" value="NDTGRKCS98T4L" />
        <input
          type="image"
          src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif"
          name="submit"
          title="PayPal - The safer, easier way to pay online!"
          alt="Donate with PayPal button"
        />
        <img
          alt=""
          src="https://www.paypal.com/en_LT/i/scr/pixel.gif"
          width="1"
          height="1"
        />
      </form>
    </main>
  );
}

