"use client";
import { useEffect, useState } from "react";

export default function Home() {
  const [pricePerLiter, setPricePerLiter] = useState("");
  const [litersPer100Km, setLitersPer100Km] = useState("");
  const [distance, setDistance] = useState("");
  const [chebrantai, setChebrantai] = useState("");
  const [pricePerKilometer, setPricePerKilometer] = useState(0);
  const [pricePerTotalDistance, setPricePerTotalDistance] = useState(0);
  const [pricePerChebrantas, setPricePerChebrantas] = useState(0);

  useEffect(() => {
    setPricePerKilometer(pricePerTotalDistance / Number(distance));
    setPricePerTotalDistance(
      (Number(pricePerLiter) / 10000) *
        (Number(distance) * Number(litersPer100Km))
    );
    setPricePerChebrantas(pricePerTotalDistance / Number(chebrantai));
  }, [
    pricePerLiter,
    litersPer100Km,
    distance,
    chebrantai,
    pricePerKilometer,
    pricePerTotalDistance,
  ]);

  return (
    <main className="flex flex-col justify-center items-center h-screen text-white bg-gray-800">
      {/* <div className="flex justify-center items-center min-h-screen bg-gray-100"> */}
      <form
        onSubmit={() => null}
        className="p-8 w-full max-w-md rounded-lg shadow-lg"
      >
        <div className="mb-4">
          <label
            htmlFor="pricePerLiter"
            className="block mb-2 text-sm font-bold"
          >
            Litro kaina (€*100/l):
          </label>
          <input
            type="number"
            inputMode="numeric"
            id="pricePerLiter"
            placeholder="154"
            value={pricePerLiter}
            onChange={(e) => setPricePerLiter(e.target.value)}
            className="px-3 py-2 w-full leading-tight text-gray-700 rounded border shadow appearance-none focus:outline-none focus:shadow-outline bg-slate-400"
          />
        </div>
        <div className="mb-4">
          <label
            htmlFor="litersPer100km"
            className="block mb-2 text-sm font-bold"
          >
            Ėdrumas (l/100km):
          </label>
          <input
            type="number"
            inputMode="numeric"
            placeholder="10"
            id="litersPer100km"
            value={litersPer100Km}
            onChange={(e) => setLitersPer100Km(e.target.value)}
            className="px-3 py-2 w-full leading-tight text-gray-700 rounded border shadow appearance-none bg-slate-400 focus:outline-none focus:shadow-outline"
          />
        </div>
        <div className="mb-4">
          <label htmlFor="people" className="block mb-2 text-sm font-bold">
            Chebrantai (vnt.):
          </label>
          <input
            type="number"
            inputMode="numeric"
            placeholder="3"
            id="people"
            value={chebrantai}
            onChange={(e) => setChebrantai(e.target.value)}
            className="px-3 py-2 w-full leading-tight text-gray-700 rounded border shadow appearance-none bg-slate-400 focus:outline-none focus:shadow-outline"
          />
        </div>
        <div className="mb-4">
          <label htmlFor="distance" className="block mb-2 text-sm font-bold">
            Atstumas (km):
          </label>
          <input
            type="number"
            id="distance"
            placeholder="200"
            inputMode="numeric"
            value={distance}
            onChange={(e) => setDistance(e.target.value)}
            className="px-3 py-2 w-full leading-tight text-gray-700 rounded border shadow appearance-none focus:outline-none focus:shadow-outline bg-slate-400"
          />
        </div>
        <div className="mb-4">
          <label htmlFor="distance" className="block mb-2 text-sm font-bold">
            Jėvrėjai kilometrui (€/km):
          </label>
          <p
            id="distance"
            className="px-3 py-2 w-full leading-tight text-gray-700 rounded border shadow appearance-none focus:outline-none focus:shadow-outline bg-slate-400"
          >
            {pricePerKilometer.toFixed(2)}
          </p>
        </div>
        <div className="mb-4">
          <label htmlFor="distance" className="block mb-2 text-sm font-bold">
            Jėvrėjai iš viso (€/{distance} km):
          </label>
          <p className="px-3 py-2 w-full leading-tight text-gray-700 rounded border shadow appearance-none bg-slate-400 focus:outline-none focus:shadow-outline">
            {pricePerTotalDistance.toFixed(2)}
          </p>
        </div>
        <div className="mb-4">
          <label htmlFor="distance" className="block mb-2 text-sm font-bold">
            Jėvrėjai chebrantui (€/vnt.):
          </label>
          <p
            id="distance"
            className="px-3 py-2 w-full leading-tight text-red-900 bg-red-300 rounded border shadow appearance-none focus:outline-none focus:shadow-outline"
          >
            {pricePerChebrantas.toFixed(2)}
          </p>
        </div>
      </form>
      {/* </div> */}
    </main>
  );
}
