import './globals.css'
import { Metadata } from 'next'
import Navbar from './navbar'

export const metadata: Metadata = {
  title: "Benzas",
  icons: [{ url: "/favicon.ico" }],
  viewport: "width=device-width, initial-scale=1",
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <head>
        <link rel="me" href="https://mastodon.benzas.lt/@noewoes"></link>
        <script async src="https://umami.benzas.lt/script.js" data-website-id="a02d8335-11be-45c4-baff-845c5dd08cee"></script>
      </head>
      <body className='bg-gray-800 h-screen flex flex-col'>
        <Navbar></Navbar>
        {children}</body>
    </html>
  )
}
